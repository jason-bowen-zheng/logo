#include <stdio.h>
#include <ncurses.h>
#include "const.h"

void init_color(void) {
	init_pair(COLOR_EDITOR_ACTIVE_LINE, COLOR_WHITE, COLOR_BLUE);
	init_pair(COLOR_EDITOR_UNACTIVE_LINE, COLOR_WHITE, COLOR_BLACK);
	init_pair(COLOR_EDITOR_NOW_LINE, COLOR_WHITE, COLOR_CYAN);
}

int main(int argc, char *argv[]) {
	extern argv;
	initscr();
	if (has_colors() == FALSE) {
		endwin();
		printf("logo: you terminal does not support color\n");
		exit(1);
	} if !(LINES>=100 && COLS>=80) {
		endwin();
		printf("logo: your terminal size less than 100x80\n");
		exit(1)
	}
	clear();
	start_color();
	init_color();
	init_panel();
	init_menu();
	run();

}
